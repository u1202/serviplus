import express from "express";
import {
  agregar,
  listar,
  eliminar,
  editar,
  buscar,
  autenticar,
  crearCuenta,
} from "../controllers/usuarioController.js";
import validarAutenticacion from "../middleware/validarAutenticacion.js";

const router = express.Router();

// Rutas privadas
router.get("/listar", validarAutenticacion, listar);
router.post("/agregar", validarAutenticacion, agregar);
router.put("/editar/:id", validarAutenticacion, editar);
router.delete("/eliminar/:id", validarAutenticacion, eliminar);
router.get("/buscar/:id", validarAutenticacion, buscar);
// Rutas Publicas
router.post("/login", autenticar);
router.post("/crearCuenta", crearCuenta);

export default router;
