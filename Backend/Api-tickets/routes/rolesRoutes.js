import express from "express";
import {
  agregar,
  listar,
  eliminar,
  editar,
  buscar,
  comboRoles,
} from "../controllers/rolController.js";
import validarAutenticacion from "../middleware/validarAutenticacion.js";

const router = express.Router();

// Rutas privadas
router.get("/listar", validarAutenticacion, listar);
router.post("/agregar", validarAutenticacion, agregar);
router.put("/editar/:id", validarAutenticacion, editar);
router.delete("/eliminar/:id", validarAutenticacion, eliminar);
router.get("/buscar/:id", validarAutenticacion, buscar);
router.get("/combo-roles", validarAutenticacion, comboRoles);

export default router;
