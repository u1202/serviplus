import express from "express";
import dotenv from "dotenv";
import conectarDB from "./config/db.js";
import cors from "cors";

// importando archivos de ruta
import rolesRoutes from "./routes/rolesRoutes.js";
import usuarioRoutes from "./routes/usuariosRoutes.js";
import categoriaRoutes from "./routes/categoriasRoutes.js";
import ticketsRoutes from "./routes/ticketsRoutes.js";
import respuestasTicketsRoutes from "./routes/respuestasTicketsRoutes.js";
import imagenesTicketsRoutes from "./routes/imagenesTicketsRoutes.js";
import imagenesRespuestasRoutes from "./routes/imagenesRespuestasRoutes.js";

// Iniciamos el servidor de express
const app = express();
app.use(express.json()); //Para leer datos en formato JSON

// Iniciamos la variable de ambiente
dotenv.config();

// Conectarnos a la base de datos
conectarDB();

// Permitir conexiones externas con cors
// const listaBlanca = [process.env.FRONTEND_URL];
// const corsOptions = {
//   origin: function (origin, callback) {
//     if (listaBlanca.includes(origin)) {
//       //permitir acceso
//       callback(null, true);
//     } else {
//       callback(new Error("Error de permiso"));
//     }
//   },
// };

// Utilizar cors
// app.use(cors(corsOptions));
app.use(cors());

// rutas
app.use("/roles", rolesRoutes);
app.use("/usuarios", usuarioRoutes);
app.use("/categorias", categoriaRoutes);
app.use("/tickets", ticketsRoutes);
app.use("/restickets", respuestasTicketsRoutes);
app.use("/imgtickets", imagenesTicketsRoutes);
app.use("/imgrespuestas", imagenesRespuestasRoutes);

//Selecionamos el puerto por donde deseamos conectarnos
const PORT = process.env.PORT || 4000;

// Nos conectamos al servidor
app.listen(PORT, () => {
  console.log(`Servidor ejecuntanse en el puerto ${PORT}`);
});
