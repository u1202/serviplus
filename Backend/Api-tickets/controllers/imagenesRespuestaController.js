const agregar = async (req, res) => {
  console.log("Respondiendo desde el metodo Agregar desde imagenes respuesta");
};

const listar = async (req, res) => {
  console.log("Respondiendo desde el metodo listar desde imagenes respuesta");
};

const eliminar = async (req, res) => {
  console.log("Respondiendo desde el metodo eliminar desde imagenes respuesta");
};

const editar = async (req, res) => {
  console.log(
    "Respondiendo desde el metodo Actualizar desde imagenes respuesta"
  );
};

const buscar = async (req, res) => {
  console.log("Respondiendo desde el metodo buscar desde imagenes respuesta");
};

export { agregar, listar, eliminar, editar, buscar };
