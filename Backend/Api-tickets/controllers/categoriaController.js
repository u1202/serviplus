import Categoria from "../models/Categoria.js";

const agregar = async (req, res) => {
  const { nombreCategoria } = req.body;
  const exiteCategoria = await Categoria.findOne({ nombreCategoria });

  if (exiteCategoria) {
    const error = new Error("Categoria ya esta registrado en la base de datos");
    return res.status(400).json({ msg: error.message, ok: "NO" });
  }

  try {
    const categoria = new Categoria(req.body);
    const categoriaAlmacenado = await categoria.save();
    res.json({ body: categoriaAlmacenado, ok: "SI" });
  } catch (error) {
    console.log(error);
  }
};

const listar = async (req, res) => {
  try {
    const categorias = await Categoria.find();
    res.json(categorias);
  } catch (error) {
    console.log(error);
  }
};

const eliminar = async (req, res) => {
  console.log("Respondiendo desde el metodo eliminar desde categoria");
};

const editar = async (req, res) => {
  console.log("Respondiendo desde el metodo Actualizar desde categoria");
};

const buscar = async (req, res) => {
  console.log("Respondiendo desde el metodo buscar desde categoria");
};

export { agregar, listar, eliminar, editar, buscar };
