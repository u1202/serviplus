import Usuario from "../models/Usuario.js";
import generarJWT from "../helpers/generarJWT.js";

const agregar = async (req, res) => {
  // Evitar usuarios duplicados por el campo usuarioAcceso
  const { usuarioAcceso } = req.body;
  const existeUsuario = await Usuario.findOne({ usuarioAcceso });

  if (existeUsuario) {
    const error = new Error("Usuario ya esta registrado en la base de datos");
    // enviamos respuesta al servdo
    return res.status(400).json({ msg: error.message, ok: "NO" });
  }

  try {
    const usuario = new Usuario(req.body);
    const usuarioGuardado = await usuario.save();
    res.json({ body: usuarioGuardado, ok: "SI" });
  } catch (error) {
    console.log(error);
  }
};

const listar = async (req, res) => {
  try {
    const usuarios = await Usuario.find().populate("idRol", {
      nombreRol: 1,
      _id: 0,
    });
    res.json(usuarios);
  } catch (error) {
    console.log(error);
  }
};

const eliminar = async (req, res) => {
  // Recibimos los parametros de la URL
  const { id } = req.params;

  // Validamos si existe el registro a eliminar
  const usuario = await Usuario.findById(id);

  if (!usuario) {
    const error = new Error("Registro no encontrado");
    return res.status(400).json({ msg: error.message, ok: "NO" });
  }

  // Si existe el registro realizamos el proceso de eliminar
  try {
    await usuario.deleteOne();
    res.json({ msg: "Registro eliminado con exito.", ok: "SI" });
  } catch (error) {
    console.log(error);
  }
};

const editar = async (req, res) => {
  console.log("Respondiendo desde el metodo Actualizar desde usuario");
};

const buscar = async (req, res) => {
  // Recibimos los parametros de la URL
  const { id } = req.params;

  // Validamos si existe el registro a buscar
  const usuario = await Usuario.findById(id);

  if (!usuario) {
    const error = new Error("Registro no encontrado");
    return res.status(400).json({ msg: error.message, ok: "NO" });
  }

  // Si existe el registro se visualiza en pantalla
  res.json(usuario);
};

const crearCuenta = async (req, res) => {
  // Evitar usuarios duplicados por el campo usuarioAcceso
  const { usuarioAcceso } = req.body;
  const existeUsuario = await Usuario.findOne({ usuarioAcceso });

  if (existeUsuario) {
    const error = new Error("Usuario ya esta registrado en la base de datos");
    // enviamos respuesta al servdo
    return res.status(400).json({ msg: error.message, ok: "NO" });
  }

  try {
    const usuario = new Usuario(req.body);
    const usuarioGuardado = await usuario.save();
    res.json({
      body: usuarioGuardado,
      ok: "SI",
      msg: "Registro creado correctamente",
    });
  } catch (error) {
    console.log(error);
  }
};

const autenticar = async (req, res) => {
  const { usuarioAcceso, claveAcceso } = req.body;

  // comprobar si el usuario existe
  const usuario = await Usuario.findOne({ usuarioAcceso });
  if (!usuario) {
    const error = new Error("Usuario no existe");
    return res
      .status(400)
      .json({ msg: error.message, ok: "Usuario no existe" });
  }

  // Validar que la contraseña exista

  if (await usuario.comprobarClave(claveAcceso)) {
    res.json({
      _id: usuario._id,
      nombresUsuario: usuario.nombresUsuario,
      usuarioAcceso: usuario.usuarioAcceso,
      tokenJwt: generarJWT(usuario._id),
    });
  } else {
    const error = new Error("Usuario o Contraseña incorrecta");
    return res
      .status(400)
      .json({ msg: error.message, ok: "Contraseña incorrecta" });
  }
};

export { agregar, listar, eliminar, editar, buscar, crearCuenta, autenticar };
