const agregar = async (req, res) => {
  console.log("Respondiendo desde el metodo Agregar desde respuestaTicket");
};

const listar = async (req, res) => {
  console.log("Respondiendo desde el metodo listar desde respuestaTicket");
};

const eliminar = async (req, res) => {
  console.log("Respondiendo desde el metodo eliminar desde respuestaTicket");
};

const editar = async (req, res) => {
  console.log("Respondiendo desde el metodo Actualizar desde respuestaTicket");
};

const buscar = async (req, res) => {
  console.log("Respondiendo desde el metodo buscar desde respuestaTicket");
};

export { agregar, listar, eliminar, editar, buscar };
