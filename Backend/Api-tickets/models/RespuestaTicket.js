import mongoose from "mongoose";

const respuestaTicketSchema = mongoose.Schema(
  {
    idTicket: {
      type: mongoose.Schema.Types.ObjectId,
      rel: "Ticket",
      require: true,
    },

    idUsuario: {
      type: mongoose.Schema.Types.ObjectId,
      rel: "Usuario",
      require: true,
    },

    fechaRespuestaTicket: {
      type: Date,
      require: true,
      trim: true,
    },

    horaRespuestaTicket: {
      type: Date,
      require: true,
      trim: true,
    },

    descripcionRespuestaTicket: {
      type: String,
      require: true,
      trim: true,
    },
  },
  { timestamps: true }
);

const RespuestaTicket = mongoose.model(
  "RespuestaTicket",
  respuestaTicketSchema
);
export default RespuestaTicket;
