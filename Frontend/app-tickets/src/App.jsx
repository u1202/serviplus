import React, { Fragment } from "react";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import CrearCuenta from "./pages/auth/CrearCuenta";
import Login from "./pages/auth/Login";
import RolesAdmin from "./pages/config/RolesAdmin";
import DashBoard from "./pages/DashBoard";
import RolesCrear from "./pages/config/RolesCrear";
import RolesEditar from "./pages/config/RolesEditar";
import UsuariosAdmin from "./pages/config/UsuariosAdmin";
import UsuariosCrear from "./pages/config/UsuariosCrear";

function App() {
  return (
    <Fragment>
      <Router>
        <Routes>
          <Route path="/" exact element={<Login />} />
          <Route path="/crear-cuenta" exact element={<CrearCuenta />} />
          <Route path="/menu-principal" exact element={<DashBoard />} />
          <Route path="/roles-admin" exact element={<RolesAdmin />} />
          <Route path="/roles-crear" exact element={<RolesCrear />} />
          <Route path="/roles-editar/:id" exact element={<RolesEditar />} />
          <Route path="/usuarios-admin" exact element={<UsuariosAdmin />} />
          <Route path="/usuarios-crear" exact element={<UsuariosCrear />} />
        </Routes>
      </Router>
    </Fragment>
  );
}

export default App;
