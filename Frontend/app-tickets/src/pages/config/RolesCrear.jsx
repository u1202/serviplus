import React, { useEffect, useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import ContentHeader from "../../components/ContentHeader";
import Footer from "../../components/Footer";
import Navbar from "../../components/Navbar";
import SidebarContainer from "../../components/SidebarContainer";
import APIInvoke from "../../helpers/APIInvoke.js";
import mensajeConfirmacion from "../../helpers/Mensajes";

const RolesCrear = () => {
  // Parte dinamica
  const navigate = useNavigate();

  const [rol, setRol] = useState({
    nombre: "",
    estado: 1,
  });

  const { nombre } = rol;

  const onChange = (e) => {
    setRol({
      ...rol,
      [e.target.name]: e.target.value,
    });
  };

  useEffect(() => {
    document.getElementById("nombre").focus();
  }, []);

  const onSubmit = (e) => {
    e.preventDefault();
    crearRol();
  };

  const crearRol = async () => {
    const body = {
      nombreRol: rol.nombre,
      estadoRol: rol.estado,
    };

    const response = await APIInvoke.invokePOST("/roles/agregar", body);
    if (response.ok === "SI") {
      mensajeConfirmacion("success", response.msg);
      setRol({
        nombre: "",
      });
      navigate("/roles-admin");
    } else {
      mensajeConfirmacion("error", response.msg);
      setRol({
        nombre: "",
        estado: 1,
      });
    }
  };

  return (
    <>
      <main className="main" id="main">
        <Navbar></Navbar>
        <SidebarContainer></SidebarContainer>

        <ContentHeader
          titulo={"Roles"}
          breadCrumb1={"Configuracion"}
          breadCrumb2={"Roles"}
          breadCrumb3={"Crear Roles"}
          ruta={"/roles-admin"}
        />

        <section className="section">
          <div className="row">
            <div className="col-lg-12">
              <div className="card">
                <div className="card-body">
                  <h5 className="card-title">Crear Roles</h5>
                  <form onSubmit={onSubmit}>
                    <div className="row mb-3">
                      <label
                        htmlFor="inputText"
                        className="col-sm-2 col-form-label"
                      >
                        Nombre
                      </label>
                      <div className="col-sm-10">
                        <input
                          type="text"
                          className="form-control"
                          id="nombre"
                          name="nombre"
                          placeholder="Ingrese el nombre del rol."
                          value={nombre}
                          onChange={onChange}
                          required
                        />
                      </div>
                    </div>
                    <div className="row mb-3">
                      <div class="col-sm-10">
                        <button type="submit" className="btn btn-primary">
                          Guardar
                        </button>
                        &nbsp;
                        <Link
                          to={"/roles-admin"}
                          className="btn btn-default float-right"
                        >
                          Cancelar
                        </Link>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </section>
      </main>
      <Footer></Footer>
    </>
  );
};

export default RolesCrear;
