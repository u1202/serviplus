import React, { useEffect, useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import ContentHeader from "../../components/ContentHeader";
import Footer from "../../components/Footer";
import Navbar from "../../components/Navbar";
import SidebarContainer from "../../components/SidebarContainer";
import APIInvoke from "../../helpers/APIInvoke.js";
import mensajeConfirmacion from "../../helpers/Mensajes";
import config from "../../config";
import Form from "react-bootstrap/Form";

const UsuariosCrear = () => {
  // Parte dinamica
  const navigate = useNavigate();

  const [usuario, setUsuario] = useState({
    idrol: "",
    userNombre: "",
    userCelular: "",
    useremain: "",
    userdir: "",
    userAccess: "",
    userclave: "",
    estado: config.api.estadoUsuarioActivo,
  });

  const {
    idrol,
    userNombre,
    userCelular,
    useremain,
    userdir,
    userAccess,
    userclave,
  } = usuario;

  const [arregloRoles, setArregloRoles] = useState([]);

  const comboRoles = async () => {
    const response = await APIInvoke.invokeGET("/roles/combo-roles");
    // console.log(response);
    setArregloRoles(response);
  };

  const onChange = (e) => {
    setUsuario({
      ...usuario,
      [e.target.name]: e.target.value,
    });
  };

  useEffect(() => {
    comboRoles();
    document.getElementById("userNombre").focus();
  }, []);

  const onSubmit = (e) => {
    e.preventDefault();
    // crearUsuario();
  };

  // const crearUsuario = async () => {
  //   const body = {};

  //   const response = await APIInvoke.invokePOST("/usuarios/agregar", body);
  //   if (response.ok === "SI") {
  //     mensajeConfirmacion("success", response.msg);
  //     setUsuario({
  //       nombre: "",
  //     });
  //     navigate("/usuarios-admin");
  //   } else {
  //     mensajeConfirmacion("error", response.msg);
  //     setUsuario({
  //       nombre: "",
  //       estado: 1,
  //     });
  //   }
  // };

  return (
    <>
      <main className="main" id="main">
        <Navbar></Navbar>
        <SidebarContainer></SidebarContainer>

        <ContentHeader
          titulo={"Roles"}
          breadCrumb1={"Configuracion"}
          breadCrumb2={"Usuarios"}
          breadCrumb3={"Crear Usuarios"}
          ruta={"/usuarios-admin"}
        />

        <section className="section">
          <div className="row">
            <div className="col-lg-12">
              <div className="card">
                <div className="card-body">
                  <h5 className="card-title">Crear Usuarios</h5>
                  <form onSubmit={onSubmit}>
                    <div className="row mb-3">
                      <label
                        htmlFor="inputText"
                        className="col-sm-2 col-form-label"
                      >
                        Seleccionar Rol
                      </label>
                      <div className="col-sm-10">
                        <Form.Select
                          aria-label="Default select example"
                          style={{ cursor: "pointer" }}
                          id="idrol"
                          name="idrol"
                          value={idrol}
                          onChange={onChange}
                        >
                          <option>Selecionar...</option>
                          {arregloRoles.map((opcion) => (
                            <option value={opcion._id} key={opcion._id}>
                              {opcion.nombreRol}
                            </option>
                          ))}
                        </Form.Select>
                      </div>
                    </div>
                    <div className="row mb-3">
                      <label
                        htmlFor="inputText"
                        className="col-sm-2 col-form-label"
                      >
                        Nombre
                      </label>
                      <div className="col-sm-10">
                        <input
                          type="text"
                          className="form-control"
                          id="userNombre"
                          name="userNombre"
                          value={userNombre}
                          onChange={onChange}
                          required
                        />
                      </div>
                    </div>
                    <div className="row mb-3">
                      <label
                        htmlFor="inputText"
                        className="col-sm-2 col-form-label"
                      >
                        Celular
                      </label>
                      <div className="col-sm-10">
                        <input
                          type="number"
                          className="form-control"
                          id="userCelular"
                          name="userCelular"
                          value={userCelular}
                          onChange={onChange}
                          required
                        />
                      </div>
                    </div>
                    <div className="row mb-3">
                      <label
                        htmlFor="inputText"
                        className="col-sm-2 col-form-label"
                      >
                        Correo Electronico
                      </label>
                      <div className="col-sm-10">
                        <input
                          type="email"
                          className="form-control"
                          id="useremain"
                          name="useremain"
                          value={useremain}
                          onChange={onChange}
                          required
                        />
                      </div>
                    </div>
                    <div className="row mb-3">
                      <label
                        htmlFor="inputText"
                        className="col-sm-2 col-form-label"
                      >
                        Direccion
                      </label>
                      <div className="col-sm-10">
                        <input
                          type="text"
                          className="form-control"
                          id="userdir"
                          name="userdir"
                          value={userdir}
                          onChange={onChange}
                          required
                        />
                      </div>
                    </div>
                    <div className="row mb-3">
                      <label
                        htmlFor="inputText"
                        className="col-sm-2 col-form-label"
                      >
                        Nombre de Usuario
                      </label>
                      <div className="col-sm-10">
                        <input
                          type="text"
                          className="form-control"
                          id="userAccess"
                          name="userAccess"
                          value={userAccess}
                          onChange={onChange}
                          required
                        />
                      </div>
                    </div>
                    <div className="row mb-3">
                      <label
                        htmlFor="inputText"
                        className="col-sm-2 col-form-label"
                      >
                        Contraseña
                      </label>
                      <div className="col-sm-10">
                        <input
                          type="text"
                          className="form-control"
                          id="userclave"
                          name="userclave"
                          value={userclave}
                          onChange={onChange}
                          required
                        />
                      </div>
                    </div>
                    <div className="row mb-3">
                      <div class="col-sm-10">
                        <button type="submit" className="btn btn-primary">
                          Guardar
                        </button>
                        &nbsp;
                        <Link
                          to={"/usuarios-admin"}
                          className="btn btn-default float-right"
                        >
                          Cancelar
                        </Link>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </section>
      </main>
      <Footer></Footer>
    </>
  );
};

export default UsuariosCrear;
