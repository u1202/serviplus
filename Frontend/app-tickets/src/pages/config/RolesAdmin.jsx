import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import ContentHeader from "../../components/ContentHeader";
import Footer from "../../components/Footer";
import Navbar from "../../components/Navbar";
import SidebarContainer from "../../components/SidebarContainer";
import APIInvoke from "../../helpers/APIInvoke.js";
import mensajeConfirmacion from "../../helpers/Mensajes";

const RolesAdmin = () => {
  const [arregloRoles, setArregloRoles] = useState([]);

  const listadoRoles = async () => {
    const response = await APIInvoke.invokeGET(`/roles/listar`);
    // console.log(response);
    setArregloRoles(response);
  };

  useEffect(() => {
    listadoRoles();
  }, []);

  // Creamos el metodo para borrar un docuemnto de la base de datos
  const borrarRol = async (e, id) => {
    // console.log(id);
    e.preventDefault();
    const response = await APIInvoke.invokeDELETE(`/roles/eliminar/${id}`);

    if (response.ok === "SI") {
      mensajeConfirmacion("success", response.msg);
      listadoRoles();
    } else {
      mensajeConfirmacion("error", response.msg);
    }
  };

  return (
    <main className="main" id="main">
      <Navbar></Navbar>
      <SidebarContainer></SidebarContainer>

      <ContentHeader
        titulo={"Roles"}
        breadCrumb1={"Configuracion"}
        breadCrumb2={"Roles"}
        breadCrumb3={"Listado Roles"}
        ruta={"/roles-admin"}
      />

      <section className="section">
        <div className="row">
          <div className="col-lg-12">
            <div className="card">
              <div className="card-body">
                <h5 className="card-title">Listado Roles</h5>
                <div className="row mb-3">
                  <div className="col-lg-12">
                    <Link to={"/roles-crear"} className="btn btn-primary">
                      Crear
                    </Link>
                  </div>
                </div>
                <table className="table table-bordered">
                  <thead>
                    <tr>
                      <th style={{ width: "20%", textAlign: "center" }}>id</th>
                      <th style={{ width: "30%", textAlign: "center" }}>Rol</th>
                      <th style={{ width: "30%", textAlign: "center" }}>
                        Estado
                      </th>
                      <th style={{ width: "20%", textAlign: "center" }}>
                        Opciones
                      </th>
                    </tr>
                  </thead>
                  <tbody>
                    {arregloRoles.map((element) => (
                      <tr style={{ textAlign: "center" }} key={element._id}>
                        <td>{element._id}</td>
                        <td>{element.nombreRol}</td>
                        <td>
                          {element.estadoRol === 1 ? (
                            <span className="text-success">Activo</span>
                          ) : (
                            <span className="text-danger">Inactivo</span>
                          )}
                        </td>
                        <td>
                          <Link
                            to={`/roles-editar/${element._id}`}
                            class="btn btn-warning mr-2"
                            title="Editar"
                          >
                            <i class="bi bi-pencil-square"></i>
                          </Link>
                          &nbsp;
                          <button
                            onClick={(e) => borrarRol(e, element._id)}
                            type="button"
                            class="btn btn-danger"
                            title="Borrar"
                          >
                            <i class="bi bi-trash"></i>
                          </button>
                        </td>
                      </tr>
                    ))}
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </section>

      <Footer></Footer>
    </main>
  );
};

export default RolesAdmin;
