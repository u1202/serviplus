import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import ContentHeader from "../../components/ContentHeader";
import Footer from "../../components/Footer";
import Navbar from "../../components/Navbar";
import SidebarContainer from "../../components/SidebarContainer";
import APIInvoke from "../../helpers/APIInvoke.js";
import mensajeConfirmacion from "../../helpers/Mensajes";

const UsuariosAdmin = () => {
  const [arregloUsuarios, setArregloUsuarios] = useState([]);

  const listadoUsuarios = async () => {
    const response = await APIInvoke.invokeGET(`/usuarios/listar`);
    console.log(response);
    setArregloUsuarios(response);
  };

  useEffect(() => {
    listadoUsuarios();
  }, []);

  // Creamos el metodo para borrar un docuemnto de la base de datos
  const borrarUsuarios = async (e, id) => {
    // console.log(id);
    e.preventDefault();
    const response = await APIInvoke.invokeDELETE(`/usuarios/eliminar/${id}`);

    if (response.ok === "SI") {
      mensajeConfirmacion("success", response.msg);
      listadoUsuarios();
    } else {
      mensajeConfirmacion("error", response.msg);
    }
  };

  return (
    <main id="main" className="main">
      <Navbar></Navbar>
      <SidebarContainer></SidebarContainer>

      <ContentHeader
        titulo={"Usuarios"}
        breadCrumb1={"Configuracion"}
        breadCrumb2={"Listado Usuarios"}
        ruta={"/usuarios-admin"}
      />

      <section className="section">
        <div className="row">
          <div className="col-lg-12">
            <div className="card">
              <div className="card-body">
                <h5 className="card-title">Listado Usuarios</h5>
                <div className="row mb-3">
                  <div className="col-lg-12">
                    <Link to={"/usuarios-crear"} className="btn btn-primary">
                      Crear
                    </Link>
                  </div>
                </div>
                <table className="table table-bordered">
                  <thead>
                    <tr>
                      <th style={{ width: "10%", textAlign: "center" }}>id</th>
                      <th style={{ width: "10%", textAlign: "center" }}>Rol</th>
                      <th style={{ width: "20%", textAlign: "center" }}>
                        Nombres
                      </th>
                      <th style={{ width: "10%", textAlign: "center" }}>
                        Celular
                      </th>
                      <th style={{ width: "10%", textAlign: "center" }}>
                        Correo
                      </th>
                      <th style={{ width: "10%", textAlign: "center" }}>
                        Direccion
                      </th>
                      <th style={{ width: "10%", textAlign: "center" }}>
                        Usuario
                      </th>
                      <th style={{ width: "10%", textAlign: "center" }}>
                        Estado
                      </th>
                      <th style={{ width: "10%", textAlign: "center" }}>
                        Opciones
                      </th>
                    </tr>
                  </thead>
                  <tbody>
                    {arregloUsuarios.map((element) => (
                      <tr style={{ textAlign: "center" }} key={element._id}>
                        <td>{element._id}</td>
                        <td>{element.idRol.nombreRol}</td>
                        <td>{element.nombresUsuario}</td>
                        <td>{element.celularUsuario}</td>
                        <td>{element.CorreoUsuario}</td>
                        <td>{element.direccionUsuario}</td>
                        <td>{element.usuarioAcceso}</td>
                        <td>
                          {element.estadoUsuario === 1 ? (
                            <span className="text-success">Activo</span>
                          ) : (
                            <span className="text-danger">Inactivo</span>
                          )}
                        </td>
                        <td>
                          <Link
                            to={`/roles-editar/${element._id}`}
                            class="btn btn-warning mr-2"
                            title="Editar"
                          >
                            <i class="bi bi-pencil-square"></i>
                          </Link>
                          &nbsp;
                          <button
                            onClick={(e) => borrarUsuarios(e, element._id)}
                            type="button"
                            class="btn btn-danger"
                            title="Borrar"
                          >
                            <i class="bi bi-trash"></i>
                          </button>
                        </td>
                      </tr>
                    ))}
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </section>

      <Footer></Footer>
    </main>
  );
};

export default UsuariosAdmin;
