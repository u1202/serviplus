import React, { useEffect, useState } from "react";
import { Link, useNavigate, useParams } from "react-router-dom";
import ContentHeader from "../../components/ContentHeader";
import Footer from "../../components/Footer";
import Navbar from "../../components/Navbar";
import SidebarContainer from "../../components/SidebarContainer";
import APIInvoke from "../../helpers/APIInvoke.js";
import mensajeConfirmacion from "../../helpers/Mensajes";
import Form from "react-bootstrap/Form";

const RolesEditar = () => {
  // Parte dinamica
  const { id } = useParams(); //Capturamos el id del rol que vamos a modificar
  // alert(id);
  const navigate = useNavigate();

  const [rol, setRol] = useState({
    nombre: "",
    estado: "",
  });

  const { nombre, estado } = rol;

  const filaRolGuardado = async () => {
    const response = await APIInvoke.invokeGET(`/roles/buscar/${id}`);
    console.log(response);
    setRol({
      nombre: response.nombreRol,
      estado: response.estadoRol,
    });
  };

  const onChange = (e) => {
    setRol({
      ...rol,
      [e.target.name]: e.target.value,
    });
  };

  useEffect(() => {
    document.getElementById("nombre").focus();
    filaRolGuardado();
  }, []);

  const onSubmit = (e) => {
    e.preventDefault();
    editar();
  };

  const editar = async () => {
    const body = {
      nombreRol: rol.nombre,
      estadoRol: rol.estado,
    };

    const response = await APIInvoke.invokePUT(`/roles/editar/${id}`, body);
    if (response.ok === "SI") {
      mensajeConfirmacion("success", response.msg);
      setRol({
        nombre: "",
        estado: "",
      });
      navigate("/roles-admin");
    } else {
      mensajeConfirmacion("error", response.msg);
      setRol({
        nombre: "",
        estado: "",
      });
    }
  };

  return (
    <>
      <main id="main" className="main">
        <Navbar></Navbar>
        <SidebarContainer></SidebarContainer>

        <ContentHeader
          titulo={"Roles"}
          breadCrumb1={"Configuracion"}
          breadCrumb2={"Listado Editar"}
          ruta={"/roles-admin"}
        />

        <section className="section">
          <div className="row">
            <div className="col-lg-12">
              <div className="card">
                <div className="card-body">
                  <h5 className="card-title">Editar Roles</h5>
                  <form onSubmit={onSubmit}>
                    <div className="row mb-3">
                      <label
                        htmlFor="inputText"
                        className="col-sm-2 col-form-label"
                      >
                        Nombre
                      </label>
                      <div className="col-sm-10">
                        <input
                          type="text"
                          className="form-control"
                          id="nombre"
                          name="nombre"
                          placeholder="Ingrese el nombre del rol."
                          value={nombre}
                          onChange={onChange}
                          required
                        />
                      </div>
                    </div>
                    <div className="row mb-3">
                      <label
                        htmlFor="inputText"
                        className="col-sm-2 col-form-label"
                      >
                        Nombre
                      </label>
                      <div className="col-sm-10">
                        <Form.Select
                          style={{ cursor: "pointer" }}
                          aria-label="Selecione el Estado"
                          className="form-control"
                          id="estado"
                          name="estado"
                          value={estado}
                          onChange={onChange}
                        >
                          <option value="1">Activo</option>
                          <option value="2">Inactivo</option>
                        </Form.Select>
                      </div>
                    </div>
                    <div className="row mb-3">
                      <div class="col-sm-10">
                        <button type="submit" className="btn btn-warning">
                          Editar
                        </button>
                        <Link
                          to={"/roles-admin"}
                          className="btn btn-default float-right"
                        >
                          Cancelar
                        </Link>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </section>
      </main>
      <Footer></Footer>
    </>
  );
};

export default RolesEditar;
