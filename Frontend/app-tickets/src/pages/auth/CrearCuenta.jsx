import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import APIInvoke from "../../helpers/APIInvoke.js";
import config from "../../config.js";
import mensajeConfirmacion from "../../helpers/Mensajes.js";

const CrearCuenta = () => {
  // Definir estado inicial con useState
  const [usuario, setUsuario] = useState({
    idrol: "635317a2998558aad972853d",
    userNombre: "",
    userCelular: "",
    useremain: "",
    userdir: "",
    userAccess: "",
    userclave: "",
    estado: config.api.estadoUsuarioActivo,
  });

  const { userNombre, userCelular, useremain, userdir, userAccess, userclave } =
    usuario;

  const onChange = (e) => {
    setUsuario({
      ...usuario,
      [e.target.name]: e.target.value,
    });
  };

  useEffect(() => {
    document.getElementById("userNombre").focus();
  }, []);

  const onSubmit = (e) => {
    e.preventDefault();
    CrearCuenta();
  };

  const CrearCuenta = async () => {
    const body = {
      idRol: usuario.idrol,
      nombresUsuario: usuario.userNombre,
      celularUsuario: usuario.userCelular,
      CorreoUsuario: usuario.useremain,
      direccionUsuario: usuario.userdir,
      usuarioAcceso: usuario.userAccess,
      claveAcceso: usuario.userclave,
      estadoUsuario: usuario.estado,
    };

    const response = await APIInvoke.invokePOST("/usuarios/crearCuenta", body);
    // console.log(response);
    if (response.ok === "SI") {
      mensajeConfirmacion("success", response.msg);
      //Cambiar estado
      setUsuario({
        userNombre: "",
        userCelular: "",
        useremain: "",
        userdir: "",
        userAccess: "",
        userclave: "",
      });
    } else {
      mensajeConfirmacion("error", response.msg);
      setUsuario({
        userNombre: "",
        userCelular: "",
        useremain: "",
        userdir: "",
        userAccess: "",
        userclave: "",
      });
    }
  };

  return (
    <div>
      <main>
        <div className="container">
          <section className="section register min-vh-100 d-flex flex-column align-items-center justify-content-center py-4">
            <div className="container">
              <div className="row justify-content-center">
                <div className="col-lg-4 col-md-6 d-flex flex-column align-items-center justify-content-center">
                  {/* <div className="d-flex justify-content-center py-4">
                    <a
                      href="index.html"
                      className="logo d-flex align-items-center w-auto"
                    >
                      <img src="assets/img/logo.png" alt /> 
                      <span className="d-none d-lg-block">NiceAdmin</span>
                    </a>
                  </div> */}
                  {/* End Logo */}
                  <div className="card mb-3">
                    <div className="card-body">
                      <div className="pt-4 pb-2">
                        <h5 className="card-title text-center pb-0 fs-4">
                          Registrar Nuevo Usuario
                        </h5>
                        <p className="text-center small">
                          Ingrese los datos del usuario
                        </p>
                      </div>
                      <form
                        className="row g-3 needs-validation"
                        noValidate
                        onSubmit={onSubmit}
                      >
                        <div className="col-12">
                          <label htmlFor="yourName" className="form-label">
                            Nombres
                          </label>
                          <input
                            type="text"
                            className="form-control"
                            id="userNombre"
                            name="userNombre"
                            value={userNombre}
                            onChange={onChange}
                            required
                          />
                          <div className="invalid-feedback">
                            Please, enter your name!
                          </div>
                        </div>
                        <div className="col-12">
                          <label htmlFor="yourName" className="form-label">
                            Celular
                          </label>
                          <input
                            type="text"
                            className="form-control"
                            id="userCelular"
                            name="userCelular"
                            value={userCelular}
                            onChange={onChange}
                            required
                          />
                          <div className="invalid-feedback">
                            Please, enter your name!
                          </div>
                        </div>
                        <div className="col-12">
                          <label htmlFor="yourEmail" className="form-label">
                            Correo Electronico
                          </label>
                          <input
                            type="email"
                            className="form-control"
                            placeholder="Ingrese su Email"
                            id="useremain"
                            name="useremain"
                            value={useremain}
                            onChange={onChange}
                            required
                          />
                          <div className="invalid-feedback">
                            Please enter a valid Email adddress!
                          </div>
                        </div>

                        <div className="col-12">
                          <label htmlFor="yourName" className="form-label">
                            Direccion
                          </label>
                          <input
                            type="text"
                            className="form-control"
                            id="userdir"
                            name="userdir"
                            value={userdir}
                            onChange={onChange}
                            required
                          />
                          <div className="invalid-feedback">
                            Please, enter your name!
                          </div>
                        </div>

                        <div className="col-12">
                          <label htmlFor="yourUsername" className="form-label">
                            Nombre de Usuario
                          </label>
                          <div className="input-group has-validation">
                            <span
                              className="input-group-text"
                              id="inputGroupPrepend"
                            >
                              @
                            </span>
                            <input
                              type="text"
                              className="form-control"
                              placeholder="Ingrese su usuario de acceso"
                              id="userAccess"
                              name="userAccess"
                              value={userAccess}
                              onChange={onChange}
                              required
                            />
                            <div className="invalid-feedback">
                              Please choose a username.
                            </div>
                          </div>
                        </div>
                        <div className="col-12">
                          <label htmlFor="yourPassword" className="form-label">
                            Contraseña
                          </label>
                          <input
                            type="password"
                            className="form-control"
                            placeholder="Password"
                            id="userclave"
                            name="userclave"
                            value={userclave}
                            onChange={onChange}
                            required
                          />
                          <div className="invalid-feedback">
                            Please enter your password!
                          </div>
                        </div>

                        <div className="col-12">
                          <button
                            className="btn btn-primary w-100"
                            type="submit"
                          >
                            Crear Cuenta
                          </button>
                        </div>
                        <div className="col-12 text-center">
                          <p className="small mb-0">
                            <Link to={"/"}> Regresar</Link>
                          </p>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
        </div>
      </main>
      <Link
        to={"#"}
        className="back-to-top d-flex align-items-center justify-content-center"
      >
        <i className="bi bi-arrow-up-short" />
      </Link>
    </div>
  );
};

export default CrearCuenta;
