import React, { useState, useEffect } from "react";
import { Link, useNavigate } from "react-router-dom";
import APIInvoke from "../../helpers/APIInvoke.js";
import mensajeConfirmacion from "../../helpers/Mensajes.js";

const Login = () => {
  // Para redireccionar a un cliente
  const navigate = useNavigate();

  const [login, setLogin] = useState({
    usuario: "",
    pass: "",
  });

  const { usuario, pass } = login;

  const onChange = (e) => {
    setLogin({
      ...login,
      [e.target.name]: e.target.value,
    });
  };

  useEffect(() => {
    document.getElementById("usuario").focus();
  }, []);

  const onSubmit = (e) => {
    e.preventDefault();
    iniciarSesion();
  };

  const iniciarSesion = async () => {
    const body = {
      usuarioAcceso: login.usuario,
      claveAcceso: login.pass,
    };
    const response = await APIInvoke.invokePOST("/usuarios/login", body);
    // console.log(response);
    if (response.ok === "Usuario no existe") {
      mensajeConfirmacion("error", response.msg);
    } else if (response.ok === "Contraseña incorrecta") {
      mensajeConfirmacion("error", response.msg);
    } else {
      // console.log(response);
      // Eliminar los datos almacenados en localStore
      localStorage.removeItem("token");
      localStorage.removeItem("idUser");
      localStorage.removeItem("userName");
      // obtener el token
      const token = response.tokenJwt;

      // Obtenemos datos adicionales de los usuarios
      const idUsuario = response._id;
      const nombreUsuario = response.nombresUsuario;

      // Guardaos infromacion en localstore se alacena en el navegador
      localStorage.setItem("token", token);
      localStorage.setItem("idUser", idUsuario);
      localStorage.setItem("userName", nombreUsuario);

      //Reedirecciona al menu principal
      navigate("/menu-principal");
    }
  };

  return (
    <main>
      <div className="container">
        <section className="section register min-vh-100 d-flex flex-column align-items-center justify-content-center py-4">
          <div className="container">
            <div className="row justify-content-center">
              <div className="col-lg-4 col-md-6 d-flex flex-column align-items-center justify-content-center">
                <div className="d-flex justify-content-center py-4">
                  <Link
                    to={"#"}
                    className="logo d-flex align-items-center w-auto"
                  >
                    <span className="d-none d-lg-block">
                      <b>App</b> Tickets
                    </span>
                  </Link>
                </div>

                <div className="card mb-3">
                  <div className="card-body">
                    <div className="pt-4 pb-2">
                      <h5 className="card-title text-center pb-0 fs-4">
                        Iniciar Sesion
                      </h5>
                      <p className="text-center small">
                        Bienvenido, Digite sus credenciales
                      </p>
                    </div>
                    <form
                      className="row g-3 needs-validation"
                      noValidate
                      onSubmit={onSubmit}
                    >
                      <div className="col-12">
                        <label htmlFor="yourUsername" className="form-label">
                          Usuario
                        </label>
                        <div className="input-group has-validation">
                          <span
                            className="input-group-text"
                            id="inputGroupPrepend"
                          >
                            @
                          </span>
                          <input
                            type="text"
                            className="form-control"
                            id="usuario"
                            name="usuario"
                            value={usuario}
                            onChange={onChange}
                            placeholder="Digite su nombre de usuario"
                            required
                          />
                          <div className="invalid-feedback">
                            Please enter your username.
                          </div>
                        </div>
                      </div>
                      <div className="col-12">
                        <label htmlFor="yourPassword" className="form-label">
                          Contraseña
                        </label>
                        <input
                          type="password"
                          className="form-control"
                          id="pass"
                          name="pass"
                          value={pass}
                          onChange={onChange}
                          placeholder="Password"
                          required
                        />
                        <div className="invalid-feedback">
                          Please enter your password!
                        </div>
                      </div>

                      <div className="col-12">
                        <button className="btn btn-primary w-100" type="submit">
                          Ingresar
                        </button>
                      </div>
                      <div className="col-12">
                        <p className="small mb-0">
                          No Tiene Cuenta?{" "}
                          <Link to={"/crear-cuenta"}>Crear Cuenta</Link>
                        </p>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    </main>
  );
};

export default Login;
