import Swal from "sweetalert2";

const mensajeConfirmacion = (icon, msg) => {
  Swal.fire({
    icon: icon,
    text: msg,
  });
};

// const mensajeConfirmacionBorrar = (titulo, text, icon, msg) => {
//   Swal.fire({
//     title: titulo,
//     text: text,
//     icon: icon,
//     showCancelButton: true,
//     confirmButtonColor: "#3085d6",
//     cancelButtonColor: "#d33",
//     confirmButtonText: "Yes, delete it!",
//   }).then((result) => {
//     if (result.isConfirmed) {
//       Swal.fire(msg);
//     }
//   });
// };

export default mensajeConfirmacion;
